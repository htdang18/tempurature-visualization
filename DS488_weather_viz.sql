drop table if exists weather cascade; 
CREATE TABLE weather (
day date,
prcp numeric,
tmax int,
tmin int);

\copy weather(day, prcp, tmax, tmin) FROM 'weather_data.csv' DELIMITER ';' CSV HEADER;



