import psycopg2

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt 

import numpy as np
# from scipy.interpolate import make_interp_spline

#Total annual precipitation
connectionString = "dbname='htdang18_db' user='htdang18'"
connection = psycopg2.connect(connectionString)
cursor = connection.cursor()

sqlStatement = " SELECT EXTRACT (year FROM day) AS year, SUM(prcp) AS total_prcp FROM weather GROUP BY EXTRACT (year FROM day);" 
cursor.execute(sqlStatement) 

records = cursor.fetchall()

year = []
total_prcp = []

for row in records:
    year.append(row[0])
    total_prcp.append(row[1])
#
fig = plt.figure(figsize = (20, 5))
plt.ylabel("total annual precipitation")
plt.xlabel("year")
plt.scatter( year, total_prcp )
plt.plot(year, total_prcp) 
plt.savefig("weather_annual_prcp.png")

#
# year_prcp_spline = make_interp_spline(total_prcp,year)
# prcp_ = np.linspace(total_prcp.min(), total_prcp.max(), 500)
# year_ = year_prcp_spline(prcp_)

# plt.plot(total_prcp, year)
# plt.xlabel("total annual precipitation")
# plt.ylabel("year")
# plt.show()


#Annual minimum tempurature
connectionString = "dbname='htdang18_db' user='htdang18'"
connection = psycopg2.connect(connectionString)
cursor = connection.cursor()

sqlStatement = "SELECT EXTRACT (year FROM day) AS year, AVG(tmin) AS tmin FROM weather GROUP BY EXTRACT (year FROM day); " 
cursor.execute(sqlStatement) 

records = cursor.fetchall()

year = []
tmin = []

for row in records:
    year.append(row[0])
    tmin.append(row[1])

fig = plt.figure(figsize = (20, 5))
plt.ylabel("annual minimum tempurature")
plt.xlabel("year")
plt.scatter( year, tmin)
plt.plot(year, tmin) 
plt.savefig("weather_annual_tmin.png")

#Annual maximum tempurature
connectionString = "dbname='htdang18_db' user='htdang18'"
connection = psycopg2.connect(connectionString)
cursor = connection.cursor()

sqlStatement = "SELECT EXTRACT (year FROM day) AS year, AVG(tmax) AS tmin FROM weather GROUP BY EXTRACT (year FROM day); " 
cursor.execute(sqlStatement) 

records = cursor.fetchall()

year = []
tmax = []

for row in records:
    year.append(row[0])
    tmax.append(row[1])

fig = plt.figure(figsize = (20, 5))
plt.ylabel("annual maximum tempurature")
plt.xlabel("year")
plt.scatter( year, tmax )
plt.plot(year, tmax) 
plt.savefig("weather_annual_tmax.png")






